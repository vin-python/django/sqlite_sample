from django import template
import datetime
register = template.Library()

#custom tag
@register.simple_tag(name="get_date")
def get_date():
    now = datetime.datetime.now()
    return now

# custom tag filter
@register.filter
def quotes(value):
    s = '"'+str(value)+'"'
    return s