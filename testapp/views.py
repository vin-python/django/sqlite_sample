from django.shortcuts import render
from django.http import HttpResponse
from testapp.models import Employee
# Create your views here.

def greetingPage(request):
    s = "<h1> Hello Welcome to first view of testapp v1.0 </h1> <p> This is Lading Page </p>"
    return HttpResponse(s)

def showContact(request):
    s = "<h1> Contact Page </h1>"
    s += "<p> Web: testapp.ai </p>"
    s += "<p> Mob: 90xxxxx12 </p>"
    s += "<p> Email: hi@testapp.ai </p>"
    return HttpResponse(s)

def aboutPage(request):
    msg  = "this is an about page"
    return render(request, 'testapp/about.html', {"message": msg})

def employeeInfo(request):
    employees = Employee.objects.all()
    data = {"employees": employees}
    res = render(request, 'testapp/employees_info.html', data)
    return(res)