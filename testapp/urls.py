from django.conf.urls import url
from testapp import views
urlpatterns = [
    url('about', views.aboutPage),
    url('contact', views.showContact),
    url('employee', views.employeeInfo),
    url('^$', views.greetingPage),
]