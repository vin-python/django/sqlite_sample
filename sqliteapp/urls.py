"""sqliteapp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))


    ^ landing
    $ ending
"""
from django.contrib import admin
from django.conf.urls import url,include
from django.urls import path
# from testapp import views as tviews
# from exam import views as eviews

urlpatterns = [
    path('admin/', admin.site.urls),
    url('testapp', include('testapp.urls')), #redirect end point having testapp:  testapp(package), urls(module)
    url('exam', include('exam.urls')), #redirect end point having exam:  exam(package), urls(module)
    url('brm', include('brm.urls')), # redirect brm to brm package(app)

    # path('hello', tviews.greetingPage)
    # path('test', eviews.showTest),
    # path('result', eviews.showResult),
    # path('contact/', tviews.showContact),
    # path('about/', tviews.aboutPage),
    # url('^$', tviews.greetingPage), # default page is greeting page
    # path('admin/', admin.site.urls),
]
