virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt

1: Django==2.2.7
__________________________________________________
    Django==2.2.7
    pytz==2020.5
    sqlparse==0.4.1




# check django is installed or not  and its version
django-admin --version

# create project in django

django-admin startproject sqliteapp

# run server 

python manage.py runserver
# http://127.0.0.1:8000/

# if you want to change port
python manage.py runserver 8001
# http://127.0.0.1:8001/

# create app 

python manage.py startapp testapp
# register this app (testapp) in settings.py

# edit views.py (function/class) ==  controller


