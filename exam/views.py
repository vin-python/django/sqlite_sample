from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def showTest(request):
    que = "Who developed C Language?"
    a = "Ken Thomson"
    b= "Dennis Ritchie"
    c = "Bjarne Stroustrup"
    d = "James Gosling"
    level = "Easy"
    data = {"que": que, "a": a, "b": b, "c": c, "d": d, "level":level}
    # res = render(request, 'exam/test.html', context=data)
    res = render(request, 'exam/test.html', data)
    return res
    # s = "<h1> Show Test Page</h1>"
    # return HttpResponse(s)


def showResult(request):
    s = "<h1> Show Result Page</h1>"
    return HttpResponse(s)

def helloExam(request):
    s = "<h1> Hello @sExam</h1>"
    return HttpResponse(s)