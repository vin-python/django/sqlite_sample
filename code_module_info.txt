django-admin startproject sqliteapp
---------------------------------------------------------------------------------
sqliteapp
    sqliteapp   => python Package
        __init__.py => It make a folder as package
        settings.py => Web Content Register
        urls.py => URL pattern, Python Function
        wsgi.py => DO NOT CHANGE ANYTHING IN THIS
    manage.py => Python Program to run server(DO NOT CHANGE)

python manage.py runserver
--------------------------------------------------------------------------------
Create App: name of the app is:  testapp
    python manage.py startapp testapp

    testapp
        __init__.py
        models.py
        views.py
        admin.py
        apps.py
        tests.py
----------------------------------------------------------------------------------

Create App: name of the app is:  exam
    python manage.py startapp exam

    exam
        __init__.py
        models.py
        views.py
        admin.py
        apps.py
        tests.py
------------------------------------------------------------------------------------
Complete Cycle of an app request

1: Create Project
2: Create Application
3: include app in settings.py
4) views.py (controller, main part of application)
    function/class
5) urls.py
6) runserver
7) send request
------------------------------------------------------------------------------

urls.py

create urls.py inside every app
    testapp
        urls.py

    exam
        urls.py

..............................................................................
MVT
- Model, View, Template

create folder "templates" inside root folder
create folder same as app name inside templates folder
    templates
        exam
        testapp

change in settings.py

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
STATIC_DIR = os.path.join(BASE_DIR, 'static')

create html file inside exam, testapp under templates

--------------------------------------------------------------------------------
Template Variables  => {{variable}}

Template Tags => {% load app_tags %}
    built-in tags: directly used in .html  / variable in views.py  used in .html


Template Filters => {{ variable | filter:arg}}

https://docs.djangoproject.com/en/2.2/ref/builtins

Custom tags:
1: create templatetags folder in app folder
2: create file app_tags.py in templatetags folder
3: define custom tag in app_tags.py
4: Use custom tag in html file(Template)


Custom template filters:
1: create templatetags folder in app folder
2: create

--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------
1: Create Project
2: Create App
3: Create template directory
4: Create static directory
5: update settings.py
6: Define Model
7: Generate Database
8: Using Admin App
9: Create Views, HTML,CSS
10: Set URLs
11: Run Server
12: Send requests
________________________________________________________________________________________________________
# Database Connection

python manage.py shell
from django.db import connection
conn = connection.cursor()

Model: models is a python class in django(python class)
Generate SQL => make migration
Execute SQL => migrate
Table Name: AppName+"_"+ className(lowercase)   #e.g: testapp_employee
id field generated automatically along with field name
..........................
    python manage.py makemigrations # all
    python manage.py makemigrations testapp #appName
    python manage.py migrate # all
    python manage.py migrate testapp
.............................
# make migrations for the app (generate sql statement to create table)
python manage.py makemigrations testapp

    Migrations for 'testapp':
      testapp/migrations/0001_initial.py
        - Create model Employee

# to check sql command used in make migration (only to view sql statement, not necessary)
python manage.py sqlmigrate testapp 0001

    BEGIN;
    --
    -- Create model Employee
    --
    CREATE TABLE "testapp_employee" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "eno" integer NOT NULL, "ename" varchar(30) NOT NULL, "esal" real NOT NULL, "eaddr" varchar(60) NOT NULL);
    COMMIT;

# migrate
python manage.py migrate
# it will migrate all the models
Operations to perform:
  Apply all migrations: admin, auth, contenttypes, sessions, testapp
Running migrations:
  Applying contenttypes.0001_initial... OK
  Applying auth.0001_initial... OK
  Applying admin.0001_initial... OK
  Applying admin.0002_logentry_remove_auto_add... OK
  Applying admin.0003_logentry_add_action_flag_choices... OK
  Applying contenttypes.0002_remove_content_type_name... OK
  Applying auth.0002_alter_permission_name_max_length... OK
  Applying auth.0003_alter_user_email_max_length... OK
  Applying auth.0004_alter_user_username_opts... OK
  Applying auth.0005_alter_user_last_login_null... OK
  Applying auth.0006_require_contenttypes_0002... OK
  Applying auth.0007_alter_validators_add_error_messages... OK
  Applying auth.0008_alter_user_username_max_length... OK
  Applying auth.0009_alter_user_last_name_max_length... OK
  Applying auth.0010_alter_group_name_max_length... OK
  Applying auth.0011_update_proxy_permissions... OK
  Applying sessions.0001_initial... OK

python manage.py migrate name_of_the_app

python manage.py migrate testapp

#in-built django app

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
# to check sql command used in make migration (only to view sql statement, not necessary)
python manage.py sqlmigrate admin 0001
python manage.py sqlmigrate auth 0001
python manage.py sqlmigrate contenttypes 0001
python manage.py sqlmigrate sessions 0001
python manage.py sqlmigrate messages 0001 # no migration, no table
python manage.py sqlmigrate staticfiles 0001 # no migration,  no table

------------------------------
# Create Superuser
http://127.0.0.1:8000/admin
python manage.py makemigrations admin  # create migration ignore if already created
python manage.py migrate admin
python manage.py makemigrations auth
python manage.py migrate auth

python manage.py createsuperuser
    set username and password
    Access URL: http://127.0.0.1:8000/admin
    Login to the system

-------------------------------
Connect to sqlite3 db

sqlite3 db.sqlite3  # db.sqlite3 is name of database

.tables
.schema table
-----------------------------
Database Fields using Models
Field Types:
    AutoField()
    BooleanField()
    CharField()
    DateField()
    EmailField()
    FileField()
    FloatField()
    ImageField()
    IntegerField()
    TimeField()

Create PK
    enid = models.IntegerField(primary_key=True)

Relationship Fields
    1: ForeignKey()
    2: ManyToManyField()
    3: OneToOneField()

    Many to One
    Many to Many
    One to One

___________________________________________________________________________________________________
---------------------------------------------------------------------------------------------------
Book Record Management (brm)
CRUD
    Create
    Read
    Update
    Delete

Database:
    Table: book
        id
        Title
        Author
        Price
        Publisher

Pages in BRM app
    1: View Books
    2: Edit Book
    3: Delete Book
    4: Search Book
    5: New Book


urls.py
    brm/view-books
    brm/edit-book
    brm/delete-book
    brm/search-book
    brm/new-book
    brm/add
    brm/edit
    brm/search
...............................
1: create app
2: create templates and static directories
3: set settings.py
4: create models
5: makemigrations and migrate
6: create html css view  # bussiness logic
7: set urls
8: run server
9: send requests


python manage.py startapp brm
    brm
        __init__.py
        amdin.py
        apps.py
        models.py
        tests.py
        views.py

Add 'brm' in INSTALLED_APPS (settings.py)
inside templates create brm folder

create model

python manage.py makemigrations brm
    Migrations for 'brm':
      brm/migrations/0001_initial.py
        - Create model Book

# to check sql command used in make migration (only to view sql statement, not necessary)
python manage.py sqlmigrate brm 0001
    BEGIN;
    --
    -- Create model Book
    --
    CREATE TABLE "brm_book" ("id" integer NOT NULL PRIMARY KEY AUTOINCREMENT, "title" varchar(100) NOT NULL, "price" real NOT NULL, "author" varchar(100) NOT NULL, "publisher" varchar(100) NOT NULL);
    COMMIT;

python manage.py migrate brm
    Operations to perform:
        Apply all migrations: brm
    Running migrations:
      Applying brm.0001_initial... OK

........................................
create a forms.py inside brm   (app folder)

security token in form
csrf: Cross-Site Request Forgery


..................................................
Login System
1: Login Page
2: Logout
3: Restricting user to access app(pages)
4: Session Variable
----------------------
Auth
    auth_user
        username, password, email, firstname, lastname,



python manage.py makemigrations brm
    Migrations for 'brm':
      brm/migrations/0002_brmuser.py
        - Create model Brmuser





















