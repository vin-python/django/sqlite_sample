from django.conf.urls import url
from brm import views

urlpatterns = [
    url('new-book', views.newBook),
    url('add', views.addBook),
    url('view-books', views.viewBooks),
    url('edit-book', views.editBook),
    url('edit', views.editBookAction),
    url('delete-book', views.deleteBook),
    url('search-book', views.searchBook),
    url('booksearch', views.bookSearch),
    url('user-login', views.userLogin),
    url('user-logout', views.userLogout)
]