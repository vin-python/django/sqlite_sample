from django.contrib import admin
from brm.models import Book, Brmuser
# Register your models here.
admin.site.register(Book)
admin.site.register(Brmuser)