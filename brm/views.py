from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from brm.forms import NewBookForm, SearchForm
from brm import models
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required

@login_required(login_url="brm/user-login")
def viewBooks(request):
    books_all = models.Book.objects.all()
    username = request.session["username"]
    print("username :::  ",username)
    res = render(request, 'brm/view_book.html', {"books": books_all, "username": username})
    return res

@login_required(login_url="brm/user-login")
def newBook(request):
    form = NewBookForm()
    res = render(request, 'brm/new_book.html', {"form": form})
    return res

@login_required(login_url="brm/user-login")
def addBook(request):
    if request.method == "POST":
        form = NewBookForm(request.POST)
        book = models.Book()
        book.title = form.data["title"]
        book.price = form.data["price"]
        book.author = form.data["author"]
        book.publisher = form.data["publisher"]
        book.save()
    # s = "Record Saved <br/> <a href='/brm/view-books'> View All Books </a>"
    return HttpResponseRedirect("brm/view-books")

@login_required(login_url="brm/user-login")
def editBook(request):
    book = models.Book.objects.get(id=request.GET["bookid"])
    fields = {"title": book.title, "price": book.price, "author": book.author, "publisher": book.publisher}
    form = NewBookForm(initial=fields)
    res = render(request, 'brm/edit_book.html', {"form": form, "book": book})
    return res
    # return HttpResponse("pass")

@login_required(login_url="brm/user-login")
def editBookAction(request):
    if request.POST:
        # print(request.POST)
        form = NewBookForm(request.POST)
        # print(form)
        book = models.Book()
        book.id = request.POST["bookid"]
        book.title = form.data["title"]
        book.price = form.data["price"]
        book.author = form.data["author"]
        book.publisher = form.data["publisher"]
        book.save()

    return HttpResponseRedirect("brm/view-books")

@login_required(login_url="brm/user-login")
def deleteBook(request):
    bookid = request.GET["bookid"]
    book = models.Book.objects.filter(id=bookid)
    book.delete()
    return HttpResponseRedirect("brm/view-books")

@login_required(login_url="brm/user-login")
def searchBook(request):
    form = SearchForm()
    res = render(request, 'brm/search_book.html', {"form": form})
    return res


@login_required(login_url="brm/user-login")
def bookSearch(request):
    form = SearchForm(request.POST)
    books = models.Book.objects.filter(title=form.data["title"])
    res = render(request, 'brm/search_book.html', {"form": form, "books": books})
    return res

def userLogin(request):
    data = {}
    if request.method=="POST":
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            request.session["username"] = username
            return HttpResponseRedirect("brm/view-books")
        else:
            data["error"] = "Username or password is incorrect"
            res = render(request, "brm/user_login.html", data)
            return res
    else:
        res = render(request, "brm/user_login.html", data)
        return res


def userLogout(request):
    logout(request)
    return HttpResponseRedirect("brm/user-login")